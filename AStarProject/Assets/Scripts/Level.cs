﻿using Pathing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

    static Level _Instance;
    public static Level Instance { get { return _Instance; } }

    private Node[,] worldNodes;
    public Tile[,] worldTiles;

    public List<GameObject> tilePrefabs;

    static Node.Pos[] directionsEven = new Node.Pos[]
            {
                new Node.Pos(1, 0),
                new Node.Pos(0, 1),
                new Node.Pos(-1, 0),
                new Node.Pos(-1, -1),
                new Node.Pos(0, -1),
                new Node.Pos(1, -1)
            };
    static Node.Pos[] directionsOdd = new Node.Pos[]
            {
                new Node.Pos(1, 0),
                new Node.Pos(1, 1),
                new Node.Pos(0, 1),
                new Node.Pos(-1, 1),
                new Node.Pos(-1, 0),
                new Node.Pos(0, -1)
            };


    Level()
    {
        _Instance = this;
    }


    static Node.Types GetRandomType()
    {
        float r = Random.value;
        if (r < 0.40f) return Node.Types.Grass;
        if (r < 0.60f) return Node.Types.Forest;
        if (r < 0.80f) return Node.Types.Mountain;
        if (r < 0.90f) return Node.Types.Desert;
        return Node.Types.Water;
    }

    GameObject GetPrefabForType(Node.Types type)
    {
        foreach(GameObject o in tilePrefabs)
        {
            if (o.name.Contains(type.ToString())) return o;
        }
        return null;
    }

    public IEnumerable<IAStarNode> GetNeighbours(Node.Pos position)
    {
        Node.Pos[] directions = position.x % 2 == 0 ? directionsEven : directionsOdd;
        foreach (var dir in directions)
        {
            int x = position.x + dir.x;
            int z = position.z + dir.z;
            if (x < 0 || z < 0) continue;
            if(x >= worldNodes.GetLength(0)) continue;
            if (z >= worldNodes.GetLength(1)) continue;
            Node next = worldNodes[x, z];
            if (!next.CanWalkOn()) continue;
            yield return next;
        }
    }


    // Use this for initialization
    void Start () {
        worldNodes = new Node[8, 8];
        worldTiles = new Tile[8, 8];
		for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                Node node = new Node(i,j, GetRandomType());
                GameObject prefab = GetPrefabForType(node.type);

                Vector3 pos;
                if (i%2 == 0)
                {
                    pos = new Vector3(j * 1f, 0, i * 0.75f);
                }
                else
                {
                    pos = new Vector3(0.5f + j * 1f, 0, i * 0.75f);
                }
                GameObject newObj = Instantiate(prefab, pos, Quaternion.identity);
                Tile tile = newObj.AddComponent<Tile>();
                tile.node = node;

                worldNodes[i, j] = node;
                worldTiles[i, j] = tile;
            }
        }
        Camera.main.transform.position = new Vector3(3f, 10f, 3.5f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
