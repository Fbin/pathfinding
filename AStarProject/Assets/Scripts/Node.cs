﻿using Pathing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : IAStarNode {
    public enum Types { Grass = 1, Forest = 3, Desert = 5, Mountain = 10, Water = -1 }
    const int AVERAGE_COST_PER_DIST= 4;

    public struct Pos
    {
        public int x;
        public int z;

        public Pos(int x = 0, int z = 0)
        {
            this.x = x;
            this.z = z;
        }

        public static Pos operator -(Pos a, Pos b)
        {
            return new Pos(a.x - b.x, a.z - b.z);
        }

        public float sqrMagnitude
        {
            get
            {
                return x * x + z * z;
            }
        }

        public float magnitude
        {
            get
            {
                return Mathf.Sqrt(sqrMagnitude);
            }
        }
    }

    Pos _position;
    Types _type;

    public Pos position { get { return _position; } }
    public Types type { get { return _type; } }

    public Node(int x, int z,Types type)
    {
        this._position.x = x;
        this._position.z = z;
        this._type = type;
    }

    public static bool CanWalkOn(Types type)
    {
        return (int)type > 0;
    }

    public bool CanWalkOn()
    {
        return CanWalkOn(this._type);
    }

    public IEnumerable<IAStarNode> Neighbours
    {
        get
        {
            return Level.Instance.GetNeighbours(this._position);
        }
    }

    public float CostTo(IAStarNode neighbour)
    {
        return (int)((Node)neighbour)._type;
    }

    public float EstimatedCostTo(IAStarNode goal)
    {
        float dist=(this._position - ((Node)goal)._position).magnitude;
        return dist * AVERAGE_COST_PER_DIST;
    }

}
