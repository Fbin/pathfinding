﻿using Pathing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour {

    private Tile start;
    private Tile end;
    private IList<IAStarNode> path;

    private void OnGUI()
    {
        GUI.Label(new Rect(50, 50, 200, 50), "left mouse click = start point\r\right mouse click = end point");
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;

		if(Input.GetMouseButtonDown(0))
        {
            Vector3 pos = Input.mousePosition;
            pos.z = 10f;
            pos = Camera.main.ScreenToWorldPoint(pos);
            Physics.Raycast(Camera.main.transform.position, pos - Camera.main.transform.position, out hit);
            CheckClick(hit, ref start);
        }
        if (Input.GetMouseButtonDown(1))
        {
            Vector3 pos = Input.mousePosition;
            pos.z = 10f;
            pos = Camera.main.ScreenToWorldPoint(pos);
            Physics.Raycast(Camera.main.transform.position, pos - Camera.main.transform.position, out hit);
            CheckClick(hit, ref end);
        }
    }

    private void PaintPath()
    {
        if(start != null && end != null)
        {
            start.transform.position = new Vector3(start.transform.position.x, 0.5f, start.transform.position.z);
            start.tileMaterial.color = new Color(0f, 1f, 0f);
            end.transform.position = new Vector3(end.transform.position.x, 0.5f, end.transform.position.z);
            end.tileMaterial.color = new Color(0f, 1f, 0f);

            if(path!=null)
            {
                for (int i = 1; i < path.Count - 1; i++)
                {
                    Level.Instance.worldTiles[((Node)path[i]).position.x, ((Node)path[i]).position.z].transform.position += new Vector3(0f, 0.5f, 0f);
                    Level.Instance.worldTiles[((Node)path[i]).position.x, ((Node)path[i]).position.z].tileMaterial.color = new Color(1f, 0f, 0f);
                }
            }
        }
    }

    private void ResetPath()
    {
        foreach (Tile n in Level.Instance.worldTiles)
        {
            n.transform.position = new Vector3(n.transform.position.x, 0f, n.transform.position.z);
            n.tileMaterial.color = new Color(1f, 1f, 1f);
        }
        path = null;
    }

    private void CheckClick(RaycastHit hit, ref Tile tile)
    {
        if (hit.collider)
        {
            if(hit.collider.GetComponent<Tile>().node.type != Node.Types.Water)
            {
                if (tile != null)
                {
                    tile.transform.Translate(0f, -0.5f, 0f);
                    tile.tileMaterial.color = new Color(1f, 1f, 1f);
                }
                if (tile == hit.collider.GetComponent<Tile>())
                {
                    tile = null;
                    ResetPath();
                }
                else
                    tile = hit.collider.GetComponent<Tile>();
                if (tile != null)
                {
                    tile.transform.Translate(0f, 0.5f, 0f);
                    tile.tileMaterial.color = new Color(0f, 1f, 0f);
                }
            }
        }
        else
        {
            tile = null;
            ResetPath();
        }
        if (DoesPathExist())
            PaintPath();
    }

    private bool DoesPathExist()
    {
        if (start != null && end != null)
        {
            if (path != null)
            {
                ResetPath();
            }
            path = AStar.GetPath(start.node, end.node);
            return true;
        }
        return false;
    }
}
